const {User} = require("../../models");
const bcrypt = require('bcrypt');
const {errors} = require("../../helpers");
const jwt = require("jsonwebtoken");

const storeUser = async (req, res)=>{
    errors.showErrors(req,res)
    try{
        // const user = await User.create(req.body)
        const validate = await User.findOne({where:{email:req.body.email}})
        if(validate){
            return res.status(401).json({msg:"El usuario ya existe"})
        }
        const user =  User.build(req.body)
        const salt = await bcrypt.genSalt(10)
        user.password = await bcrypt.hash(req.body.password,salt)
        await user.save()
        const token = jwt.sign({
            name:user.name,
            email:user.email,
            id:user.id
        }, 'SECRET', {
            expiresIn: '8h'
        })
        return res.status(200).json({msg:"Usuario creado con exito", token, user:{
            ...user, password:undefined
            }})
    }catch (e) {
        console.log(e)
        return res.status(500).json({msg:"Fallo"})
    }

}
module.exports = {
    storeUser
}
