const jwt =  require('jsonwebtoken')
const bcrypt =  require('bcrypt')


const {User} = require("../../models")
const {errors} = require("../../helpers");

const auth =  async(req,res)=>{
    errors.showErrors(req,res)
    const {email, password} = req.body
    try{
        const validate = await User.findOne({where:{email}})
        if(!validate){
            return res.status(401).json({msg:"El usuario o la contraseña no es correcto"})
        }

        if(!bcrypt.compareSync(password, validate.password)){
            return res.status(401).json({msg:"El usuario o la contraseña no es correcto"})
        }
        const token = jwt.sign({
            name:validate.name,
            email:validate.email,
            id:validate.id
        }, 'SECRET', {
            expiresIn: '8h'
        })
        return res.status(200).json({
            msg:'Logueado',
            token,
            user:{
                ...validate.dataValues,
                password:undefined
            }
        })

    }catch (e) {
        console.log(e)
    }

}


module.exports = {
    auth
}
