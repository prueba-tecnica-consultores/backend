const {Movie, Character, CharacterMovies, CategoryMovies} = require("../../models");
const {errors} = require("../../helpers");
const storeMovie = async (req,res) => {
    errors.showErrors(req,res)
  try {
      const movie = await Movie.create(req.body)
      if(movie){
          return res.status(200).send({msg:'Guardado', movie})
      }
     return res.status(400).send({msg:'Ha ocurrido un error'})
  }catch (e) {
      console.log(e);
      return res.status(500).send({msg:'Server Error'})
  }
}
const getMovie = async(req,res)=>{
    try {
        const movie =  await Movie.findOne({
            where:{
                id:req.params.id
            },
            include: 'Characters'
        })
        if(!movie){
            return res.status(404).send({msg:'No existe EL personaje'})
        }
        return res.status(200).send({movie})
    }catch (e) {
        console.log(e)
        return res.status(500).send({
            msg:'error en el servidor'
        })
    }
}
const getMovies = async (req,res)=>{
    const {category_id,title,order} = req.query
    try {
        const movies = await  Movie.sequelize.query(`SELECT DISTINCT Movies.id, title, rate, image, date FROM Movies,CategoryMovies WHERE IsNull(Movies.destroyTime)  AND MovieId=Movies.id ${title ? `AND title LIKE '%${title}%' ` :''} ${category_id ? `AND CategoryID = ${category_id} ` :''}  ${order ? ` ORDER BY date ${order}`:''}`)
        return  res.status(200).send({
            msg:'Characters',
            movies:movies[0]
        })
    }catch (e) {
        console.log(e)
        return res.status(500).send({
            msg:'error en el servidor'
        })
    }
}
const deleteMovie = async (req, res)=>{
    try {
        const movie = await Movie.destroy({
            where:{
                id: req.params.id
            }
        })
        if(movie){
            return res.status(200).send({msg:'Borrado con exito'})
        }
        return res.status(404).send({msg:'La pelicula o serie no se encuentra'})
    }catch (e) {
        console.log(e);
        return res.status(500).send({msg:'Server Error'})
    }
}
const assignCharacter =async(req,res)=>{
    errors.showErrors(req,res)
    const {movie_id,character_id} = req.body
    try {
        const movie = await CharacterMovies.create({MovieId:movie_id,CharacterId:character_id})
        if(movie){
            return res.status(200).send({msg:'Asignado', movie})
        }
        return res.status(400).send({msg:'Ha ocurrido un error'})
    }catch (e) {
        console.log(e);
        return res.status(500).send({msg:'Server Error'})
    }
}
const assignCategory =async(req,res)=>{
    errors.showErrors(req,res)
    const {movie_id,category_id} = req.body
    try {
        const movie = await CategoryMovies.create({MovieId:movie_id,CategoryId:category_id})
        if(movie){
            return res.status(200).send({msg:'Asignado', movie})
        }
        return res.status(400).send({msg:'Ha ocurrido un error'})
    }catch (e) {
        console.log(e);
        return res.status(500).send({msg:'Server Error'})
    }
}
module.exports = {
    storeMovie,
    getMovie,
    getMovies,
    deleteMovie,
    assignCharacter,
    assignCategory
}
