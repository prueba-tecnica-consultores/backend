const {Character} = require( "../../models");
const {errors} = require( "../../helpers");
const {body} = require("express-validator");
const {Op} = require("sequelize");


const storeCharacter = async (req,res)=>{
    errors.showErrors(req,res)
    try {
        const character =  Character.build(req.body)
        await character.save()
        return res.status(200).send({
            msg:'Guardado',
            character
        })
    }catch (e) {
        console.log(e)
        return res.status(500).send({msg:'server error'})
    }
}
const getCharacter = async (req,res)=>{

    try {
        const character =  await Character.findOne({
            where:{
                id:req.params.id
            },
            include: 'Movies'
        })
        if(!character){
            return res.status(404).send({msg:'No existe EL personaje'})
        }
        return res.status(200).send({character})
    }catch (e) {
        console.log(e)
        return res.status(500).send({
            msg:'error en el servidor'
        })
    }
}
const getCharacters = async (req,res)=>{
    const {age,name,movie_id, weight} = req.query
    try {
      const characters = await  Character.sequelize.query(`SELECT DISTINCT Characters.id,name,age,history,image FROM Characters,CharacterMovies WHERE IsNull(Characters.destroyTime) AND CharacterId=Characters.id ${name ? `AND name LIKE '%${name}%' ` :''} ${age ? `AND age = ${age}`:''} ${weight ? `AND weight = ${weight}`:''}  ${movie_id ? `AND MovieId = ${movie_id}`:''}`)
       return  res.status(200).send({
           msg:'Characters',
           characters:characters[0]
       })
    }catch (e) {
        console.log(e)
        return res.status(500).send({
            msg:'error en el servidor'
        })
    }
}
const deleteCharacter = async (req,res)=>{
    try {
        const character = await Character.destroy({
            where:{
                id:req.params.id
            }
        })
        if(character){
            return res.status(200).send({
                msg:'Eliminado con exito',
            })
        }
        return res.status(404).send({
            msg:'El personaje no se encuentra',
        })
    }catch (e) {
        console.log(e)
        return res.status(500).send({
            msg:'error en el servidor'
        })
    }
}
module.exports = {
    storeCharacter,
    getCharacters,
    getCharacter,
    deleteCharacter
}
