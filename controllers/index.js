const userController =  require('./users');
const authController = require('./auth')
const characterController = require('./character')
const movieController = require('./movie')
const categoryController = require('./category')
module.exports = {
    userController,
    authController,
    characterController,
    movieController,
    categoryController
}
