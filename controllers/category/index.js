const {Category} = require("../../models");
const {errors} = require("../../helpers");

const storeCategory = async(req,res)=>{
    errors.showErrors(req,res)
    try{
        const category =  await Category.create(req.body)
        await category.save();
        return res.status(200).send({
            msg:'Guardado',
            category
        })
    }catch (e) {
        console.log(e);
        return res.status(500).send({msg:'Server Error'})
    }
}
const getCategories = async(req,res)=>{
    try{
        const categories =  await Category.findAll();
        return res.status(200).send({
            msg:'Encontrado',
            categories
        })
    }catch (e) {
        console.log(e);
        return res.status(500).send({msg:'Server Error'})
    }
}
const deleteCategory = async (req, res)=>{
    try {
        const category = await Category.destroy({
            where:{
                id: req.params.id
            }
        })
        if(category){
            return res.status(200).send({msg:'Borrado con exito'})
        }
        return res.status(404).send({msg:'La pelicula o serie no se encuentra'})
    }catch (e) {
        console.log(e);
        return res.status(500).send({msg:'Server Error'})
    }
}

module.exports = {
    storeCategory,
    getCategories,
    deleteCategory
}
