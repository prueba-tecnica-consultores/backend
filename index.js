
const express = require('express')
const {connectDB}  = require('./config/database')
const cors = require('cors');

const routes = require('./routes')
const {initTables} = require("./models");


connectDB();
initTables();

const app = express()

const port = process.env.PORT || 4000

const {users,auth, character, movie, category } =  routes


app.use(express.json());
app.use('/api/images',express.static(__dirname + '/uploads'));
// Put this statement near the top of your module
app.use(cors());
app.use('/api/user', users)
app.use('/api/auth', auth)
app.use('/api/characters', character)
app.use('/api/movie', movie)
app.use('/api/category', category)


app.listen(port,'0.0.0.0',()=>{
    console.log('Running')
})
