const express = require('express')
const { check } = require('express-validator')

const {movieController} = require('../../controllers')
const {authMiddleware, fileMiddleware} = require("../../middleware");

const router = express.Router();

var bodyParser = require('body-parser');

router.use(authMiddleware)
router.post('/',[
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    fileMiddleware,
    check('title', 'el titulo es obligatorio').not().isEmpty(),
    check('date', 'la fecha de creacion es requerida').isISO8601().toDate(),
    check('rate', 'la calificaion es requerida').isNumeric({no_symbols:true}).not().isEmpty(),
],movieController.storeMovie)
router.get('/',movieController.getMovies)
router.get('/:id', movieController.getMovie)
router.delete('/:id', movieController.deleteMovie)
router.post('/category',[
    check('category_id', 'la categoria es requerida').isNumeric({no_symbols:true}).not().isEmpty(),
    check('movie_id', 'la pelicula o serie es requerida').isNumeric({no_symbols:true}).not().isEmpty()
], movieController.assignCategory)
router.post('/character',[
    check('character_id', 'el personaje es requerido').isNumeric({no_symbols:true}).not().isEmpty(),
    check('movie_id', 'la pelicula o erie es requerida').isNumeric({no_symbols:true}).not().isEmpty()
], movieController.assignCharacter)

module.exports = router
