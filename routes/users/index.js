const express = require('express')

const { check } = require('express-validator')
const router = express.Router();

const {userController} = require('../../controllers')


router.post('/',[
    check('name', 'el nombre es obligatorio').not().isEmpty(),
    check('last_name', 'el apellido es obligatorio').not().isEmpty(),
    check('password', 'la contraseña es obligatoria').not().isEmpty().isLength({min:5, max:10}),
    check('email', 'agrega un email valido').isEmail()
],userController.storeUser)

module.exports = router
