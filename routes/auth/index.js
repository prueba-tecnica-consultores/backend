const express = require('express')

const { check } = require('express-validator')
const router = express.Router();

const {authController} = require('../../controllers')


router.post('/',[
    check('password', 'la contraseña es obligatoria').not().isEmpty(),
    check('email', 'agrega un email valido').isEmail()
],authController.auth)

module.exports = router
