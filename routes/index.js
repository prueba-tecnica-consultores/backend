const users =  require('./users');
const auth =  require('./auth');
const character = require('./character');
const movie = require('./movie');
const category = require('./category');
module.exports = {
    users,
    auth,
    character,
    movie,
    category
}
