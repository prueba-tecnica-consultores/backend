const express = require('express')
const { check } = require('express-validator')

const {characterController} = require('../../controllers')
const {authMiddleware, fileMiddleware} = require("../../middleware");

const router = express.Router();

var bodyParser = require('body-parser');

router.use(authMiddleware)
router.post('/',[
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    fileMiddleware,
    check('name', 'el nombre es obligatorio').not().isEmpty(),
    check('history', 'la historia es requerida').not().isEmpty(),
    check('age', 'la edad es requerida').isNumeric({no_symbols:true}).not().isEmpty(),
    check('weight', 'el peso es requerido').isNumeric({no_symbols:true}).not().isEmpty(),
],characterController.storeCharacter)
router.get('/',characterController.getCharacters)
router.delete('/:id', characterController.deleteCharacter)
router.get('/:id', characterController.getCharacter)

module.exports = router
