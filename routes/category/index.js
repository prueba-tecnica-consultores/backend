const express = require('express')
const { check } = require('express-validator')

const {categoryController} = require('../../controllers')
const {authMiddleware, fileMiddleware} = require("../../middleware");

const router = express.Router();

var bodyParser = require('body-parser');

router.use(authMiddleware)
router.post('/',[
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    fileMiddleware,
    check('name', 'el nombre es obligatorio').not().isEmpty(),
],categoryController.storeCategory)
router.get('/',categoryController.getCategories)
router.delete('/:id', categoryController.deleteCategory)

module.exports = router
