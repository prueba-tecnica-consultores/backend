var multer = require('multer');
const shortId = require("shortid");
var forms = multer({
    limits : { fileSize : 1024 * 1024 * 10  },
    storage: fileStorage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, __dirname+'/../../uploads')
        },
        filename: (req, file, cb) => {
            const extension = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
            cb(null, `${shortId.generate()}${extension}` );
        }
    })
});

module.exports = (req, res, next)=> {
    const upload = forms.single('image')
    upload(req,res, async (error)=>{
        if(!error){
            console.log(req.file?.filename)
            if(req.file?.filename){
                req.body.image = req.file.filename
                return next();
            }
            return res.status(400).send({msg:'La imagen no se subio correctamente, verifica e intenta de nuevo'})
        }
        console.log(error)
        return res.status(500).send({
            msg:'error al subir el archivo'
        })
    })
}
