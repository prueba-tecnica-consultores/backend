const jwt = require("jsonwebtoken");
module.exports = (req,res, next)=>{
    const authHeader = req.get('Authorization')
    if(!authHeader){
        return res.status(401).json({msg:'inicia sesion'})
    }
    try {
        const token = authHeader.split(' ')[1]
        req.user = jwt.verify(token, 'SECRET')
    }catch (e) {
        return res.status(401).json({msg:'token no valido'})
    }
    return next()
}
