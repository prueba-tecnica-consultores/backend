const {sequelize} = require("../../config/database");
const {DataTypes} = require("sequelize");
const Movie = require("../movie");
const Character = require("../character");
const CharacterMovies = sequelize.define('CharacterMovies', {
    MovieId: {
        type: DataTypes.INTEGER,
        references: {
            model: Movie, // 'Movies' would also work
            key: 'id'
        }
    },
    CharacterId: {
        type: DataTypes.INTEGER,
        references: {
            model: Character, // 'Actors' would also work
            key: 'id'
        }
    }
},{
    timestamps:true,
    paranoid: true,
    deletedAt: 'destroyTime',
});
module.exports = CharacterMovies
