const { DataTypes } = require('sequelize');
const {sequelize} = require('../../config/database')
const Movie = require('../movie')
const Category = sequelize.define('Category', {
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    image: {
        type: DataTypes.STRING,
        allowNull: false,
    },
}, {
    // Other model options go here
    tableName: 'Categories',
    timestamps:true,
    paranoid: true,
    deletedAt: 'destroyTime',

});
module.exports = Category
