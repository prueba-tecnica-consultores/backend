const { DataTypes } = require('sequelize');
const {sequelize} = require('../../config/database')
const Character = require('../character')
const Category = require('../category')
const Movie = sequelize.define('Movie', {
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    rate: {
        type: DataTypes.INTEGER
        // allowNull defaults to true
    },
    image: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    date:{
        type:DataTypes.DATE,
        allowNull:false,
    }
}, {
    // Other model options go here
    tableName: 'Movies',
    timestamps:true,
    paranoid: true,
    deletedAt: 'destroyTime',

});
module.exports = Movie
