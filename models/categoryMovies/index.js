const {sequelize} = require("../../config/database");
const {DataTypes} = require("sequelize");
const Movie = require("../movie");
const Category = require("../category");
const CategoryMovies = sequelize.define('CategoryMovies', {
    MovieId: {
        type: DataTypes.INTEGER,
        references: {
            model: Movie, // 'Movies' would also work
            key: 'id'
        }
    },
    CategoryId: {
        type: DataTypes.INTEGER,
        references: {
            model: Category, // 'Actors' would also work
            key: 'id'
        }
    }
},{
    timestamps:true,
    paranoid: true,
    deletedAt: 'destroyTime',
});
module.exports = CategoryMovies
