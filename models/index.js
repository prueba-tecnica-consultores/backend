const User = require("./users");
const Movie = require("./movie")
const Category = require("./category")
const Character = require("./character")
const CharacterMovies = require("./characterMovies")
const CategoryMovies = require("./categoryMovies")


Character.belongsToMany(Movie,{ through: CharacterMovies })
Category.belongsToMany(Movie,{ through: CategoryMovies })
Movie.belongsToMany(Character,{ through: CharacterMovies })
Movie.belongsToMany(Category,{ through: CategoryMovies })

const initTables =  async ()=>{
    try{
        await User.sync();
        await Character.sync();
        await Movie.sync();
        await Category.sync();
        await CategoryMovies.sync();
        await CharacterMovies.sync();
    }catch (e) {
        console.log(e)
    }

}
module.exports = {
    User,
    Movie,
    Category,
    Character,
    CategoryMovies,
    CharacterMovies,
    initTables
}
