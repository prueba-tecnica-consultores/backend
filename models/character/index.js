const { DataTypes } = require('sequelize');
const {sequelize} = require('../../config/database')
const Movie = require('../movie')
const Character = sequelize.define('Character', {
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    age: {
        type: DataTypes.INTEGER
        // allowNull defaults to true
    },
    history:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    weight:{
        type: DataTypes.INTEGER,
        allowNull: false
    },
    image: {
        type: DataTypes.STRING,
        allowNull: false,
    },
}, {
    // Other model options go here
    tableName: 'Characters',
    timestamps:true,
    paranoid: true,
    deletedAt: 'destroyTime',

});
module.exports = Character
