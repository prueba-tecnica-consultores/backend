const { DataTypes } = require('sequelize');
const {sequelize} = require('../../config/database')

const User = sequelize.define('User', {
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    lastName: {
        type: DataTypes.STRING
        // allowNull defaults to true
    },
    email:{
        type: DataTypes.STRING,
        allowNull: false,
        unique:true
    },
    password:{
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    // Other model options go here
    tableName: 'Users',
    timestamps:true,
    paranoid: true,
    deletedAt: 'destroyTime',
    indexes: [{ unique: true, fields: ['email'] }]

});

module.exports = User
